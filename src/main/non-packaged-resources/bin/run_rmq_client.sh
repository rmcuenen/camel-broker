#!/bin/sh

if [ -z "$JAVA_HOME" ]; then
  javaExecutable=`which javac`
  if [ -n "$javaExecutable" -a ! `expr "$javaExecutable" : '\([^ ]*\)'` = "no" ]; then
    javaExecutable=`readlink -f "$javaExecutable"`
    javaHome=`dirname $javaExecutable`
    javaHome=`expr "$javaHome" : '\(.*\)/bin'`
    JAVA_HOME="$javaHome"
  fi
fi

if [ -x "$JAVA_HOME/bin/java" ]; then
  JAVA="$JAVA_HOME/bin/java"
else
  JAVA=`which java`
fi

if [ ! -x "$JAVA" ]; then
  echo "Could not find any executable java binary. Please install java in your PATH or set JAVA_HOME"
  exit 1
fi

# SCRIPT may be an arbitrarily deep series of symlinks. Loop until we have the concrete path.
SCRIPT="$0"
while [ -h "$SCRIPT" ] ; do
  ls=`ls -ld "$SCRIPT"`
  # Drop everything prior to ->
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    SCRIPT="$link"
  else
    SCRIPT=`dirname "$SCRIPT"`/"$link"
  fi
done

# determine home
APP_HOME=`dirname "$SCRIPT"`/..

# make APP_HOME absolute
APP_HOME=`cd "$APP_HOME"; pwd`

APP_CLASSPATH=$APP_CLASSPATH:$APP_HOME/lib/*

# Special-case path variables.
case `uname` in
    CYGWIN*)
        APP_CLASSPATH=`cygpath -p -w "$APP_CLASSPATH"`
        APP_HOME=`cygpath -p -w "$APP_HOME"`
    ;;
esac

if [ "x$APP_MIN_MEM" = "x" ]; then
  APP_MIN_MEM=256m
fi
if [ "x$APP_MAX_MEM" = "x" ]; then
  APP_MAX_MEM=1g
fi
if [ "x$APP_HEAP_SIZE" != "x" ]; then
  APP_MIN_MEM=$APP_HEAP_SIZE
  APP_MAX_MEM=$APP_HEAP_SIZE
fi

# min and max heap sizes should be set to the same value to avoid
# stop-the-world GC pauses during resize.
JAVA_OPTS="$JAVA_OPTS -Xms${APP_MIN_MEM}"
JAVA_OPTS="$JAVA_OPTS -Xmx${APP_MAX_MEM}"

# reduce the per-thread stack size
JAVA_OPTS="$JAVA_OPTS -Xss256k"

# set to headless, just in case
JAVA_OPTS="$JAVA_OPTS -Djava.awt.headless=true"

# Force the JVM to use IPv4 stack
JAVA_OPTS="$JAVA_OPTS -Djava.net.preferIPv4Stack=true"

# Causes the JVM to dump its heap on OutOfMemory.
JAVA_OPTS="$JAVA_OPTS -XX:+HeapDumpOnOutOfMemoryError"
# The path to the heap dump location, note directory must exists and have enough
# space for a full heap dump.
#JAVA_OPTS="$JAVA_OPTS -XX:HeapDumpPath=$RFF_HOME/log/heapdump.hprof"

launch_service()
{
  props=$1

  exec "$JAVA" $JAVA_OPTS $APP_JAVA_OPTS -cp "$APP_CLASSPATH" $props \
      cuenen.raymond.broker.RabbitMQClient
  # exec without running it in the background, makes it replace this shell, we'll never get here...
  # no need to return something
}

# Parse any command line options.
args=`getopt vdhp:D:X: "$@"`
eval set -- "$args"

while true; do
  case $1 in
    -h)
      echo "Usage: $0 [-h]"
      exit 0
    ;;
    -D)
      properties="$properties -D$2"
      shift 2
    ;;
    -X)
      properties="$properties -X$2"
      shift 2
    ;;
    --)
      shift
      break
    ;;
    *)
      echo "Error parsing argument $1!" >&2
      exit 1
    ;;
  esac
done

# Start up the service
launch_service "$properties"

exit $?
