@ECHO OFF

SETLOCAL

IF NOT DEFINED JAVA_HOME GOTO Error

SET SCRIPT_DIR=%~dp0
FOR %%I IN ("%SCRIPT_DIR%..") DO SET APP_HOME=%%~dpfI

SET APP_CLASSPATH=%APP_CLASSPATH%;%APP_HOME%/lib/*

IF "%APP_MIN_MEM%" == "" SET APP_MIN_MEM=256m
IF "%APP_MAX_MEM%" == "" SET APP_MAX_MEM=1g

IF NOT "%APP_HEAP_SIZE%" == "" (
  SET APP_MIN_MEM=%RFF_HEAP_SIZE%
  SET APP_MAX_MEM=%RFF_HEAP_SIZE%
)

REM min and max heap sizes should be set to the same value to avoid
REM stop-the-world GC pauses during resize.
SET JAVA_OPTS=%JAVA_OPTS% -Xms%APP_MIN_MEM%
SET JAVA_OPTS=%JAVA_OPTS% -Xmx%APP_MAX_MEM%

REM reduce the per-thread stack size
SET JAVA_OPTS=%JAVA_OPTS% -Xss256k

REM Causes the JVM to dump its heap on OutOfMemory.
SET JAVA_OPTS=%JAVA_OPTS% -XX:+HeapDumpOnOutOfMemoryError
REM The path to the heap dump location, note directory must exists and have enough
REM space for a full heap dump.
REM JAVA_OPTS=%JAVA_OPTS% -XX:HeapDumpPath=$APP_HOME/log/heapdump.hprof

TITLE Camel Broker

"%JAVA_HOME%\bin\java" %JAVA_OPTS% %APP_JAVA_OPTS% %* -cp "%APP_CLASSPATH%" "cuenen.raymond.broker.Main"
GOTO Finally

:Error
ECHO JAVA_HOME environment variable must be set!
PAUSE

:Finally
ENDLOCAL
