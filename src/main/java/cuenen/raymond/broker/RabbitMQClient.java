package cuenen.raymond.broker;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RabbitMQClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQClient.class);
    private static final String EXCHANGE_NAME = "its.messages";

    private final ConnectionFactory connectionFactory = new ConnectionFactory();

    public RabbitMQClient(String host) {
        connectionFactory.setHost(host);
    }

    public void start() throws Exception {
        final Connection connection = connectionFactory.newConnection();
        final Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "topic", true, true, null);
        final String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        final QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(queueName, false, consumer);

        LOGGER.info("Waiting for messages...");
        while (true) {
            final QueueingConsumer.Delivery message = consumer.nextDelivery();
            final String messageText = new String(message.getBody());
            LOGGER.info("Received message: {}", messageText);
        }
    }

    public static void main(String[] args) throws Exception {
        String host = System.getProperty("rabbitmq.host", "localhost");
        if (args.length > 0) {
            host = args[0];
        }
        RabbitMQClient client = new RabbitMQClient(host);
        client.start();
    }
}
