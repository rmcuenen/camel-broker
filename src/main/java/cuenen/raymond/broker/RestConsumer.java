package cuenen.raymond.broker;

import akka.actor.ActorSystem;
import akka.camel.CamelExtension;
import akka.camel.CamelMessage;
import akka.camel.javaapi.UntypedConsumerActor;
import akka.dispatch.Futures;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.pattern.Patterns;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import org.apache.camel.ProducerTemplate;
import scala.concurrent.Future;
import scala.concurrent.Promise;

public class RestConsumer extends UntypedConsumerActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public String getEndpointUri() {
        return "rest:post:services/messages/{MessageType}/v1";
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof CamelMessage) {
            final CamelMessage camelMessage = (CamelMessage) message;
            log.info("[{}] Received message: {}", getSelf().path().name(),
                    camelMessage.getBodyAs(String.class, getCamelContext()));
            final String type = camelMessage.getHeaderAs("MessageType", String.class, getCamelContext());
            Patterns.pipe(new MessageExchange(getContext().system()).send(type), getContext().dispatcher()).to(getSender());
        } else {
            unhandled(message);
        }
    }

    private static class MessageExchange implements Callable<Object> {

        private final ProducerTemplate template;
        private final Promise<Object> promise = Futures.promise();
        private java.util.concurrent.Future<Object> future;

        public MessageExchange(ActorSystem system) {
            this.template = CamelExtension.get(system).template();
        }

        @Override
        public Object call() throws Exception {
            return future.get();
        }

        public Future<Object> send(String message) {
            future = template.asyncRequestBody("direct:its.requests", message);
            FutureTask task = new FutureTask(this) {

                @Override
                protected void done() {
                    try {
                        promise.success(get());
                    } catch (Exception ex) {
                        promise.failure(ex);
                    }
                }
            };
            ExecutorService executor = Executors.newFixedThreadPool(2);
            executor.execute(task);
            return promise.future();
        }
    }
}
