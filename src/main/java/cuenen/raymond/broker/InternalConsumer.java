package cuenen.raymond.broker;

import akka.camel.CamelMessage;
import akka.camel.javaapi.UntypedConsumerActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class InternalConsumer extends UntypedConsumerActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public String getEndpointUri() {
        return "activemq:topic:its.messages?asyncConsumer=true";
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof CamelMessage) {
            final CamelMessage camelMessage = (CamelMessage) message;
            log.info("[{}] Received message: {}", getSelf().path().name(),
                    camelMessage.getBodyAs(String.class, getCamelContext()));
        } else {
            unhandled(message);
        }
    }
}
