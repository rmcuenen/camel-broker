package cuenen.raymond.broker;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.camel.CamelExtension;
import akka.routing.RoundRobinPool;
import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;
import org.apache.camel.ProducerTemplate;

public class Main {

    public static void main(String[] args) throws Exception {
        String home = System.getProperty("conf.dir", System.getProperty("user.dir") + "/conf");
        System.setProperty("conf.dir", home.replace('\\', '/'));
        System.setProperty("logback.configurationFile", System.getProperty("logback.configurationFile", home + "/logback.xml"));
        System.setProperty("camel.config", System.getProperty("camel.config", home + "/camel-context.xml"));
        final ActorSystem system = ActorSystem.create("CamelBroker");
        CamelHelper.initRoutes(system);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                system.shutdown();
            }
        });
        system.actorOf(Props.create(InternalConsumer.class), "InternalConsumer");
        system.actorOf(Props.create(RequestHandler.class).withRouter(new RoundRobinPool(2)), "RequestHandler");
        system.actorOf(Props.create(RestConsumer.class), "RestConsumer");
        final ProducerTemplate template = CamelExtension.get(system).template();
        do {
            final String message = sendMessage();
            if (message == null) {
                System.exit(1);
            } else {
                System.out.println("Sending message: " + message);
                template.sendBody("direct:itsMessages", message);
                Thread.sleep(1000L);
            }
        } while (true);
    }

    private static String sendMessage() throws Exception {
        final Console c = System.console();
        String message;
        if (c == null) {
            System.out.print("Enter message: ");
            final BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
            message = r.readLine();
        } else {
            message = c.readLine("Enter message: ");
        }
        return message;
    }

}
