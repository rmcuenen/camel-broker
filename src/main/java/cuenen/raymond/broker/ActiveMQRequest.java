package cuenen.raymond.broker;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActiveMQRequest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActiveMQRequest.class);

    private static final String QUEUE_NAME = "ITS.Operations";
    private static final String MESSAGE_ID = "amq-request";

    private final ActiveMQConnectionFactory connectionFactory;

    public ActiveMQRequest(String host) {
        connectionFactory = new ActiveMQConnectionFactory("tcp://" + host + ":61616");
    }

    public void request() throws Exception {
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination destination = session.createQueue(QUEUE_NAME);
        final MessageProducer producer = session.createProducer(destination);

        final Destination tempDest = session.createTemporaryQueue();
        final MessageConsumer responseConsumer = session.createConsumer(tempDest);
        final TextMessage txtMessage = session.createTextMessage();
        txtMessage.setText("Request from ActiveMQ");
        txtMessage.setJMSReplyTo(tempDest);
        txtMessage.setJMSCorrelationID(MESSAGE_ID);
        producer.send(txtMessage);

        while (true) {
            final Message message = responseConsumer.receive();
            if (MESSAGE_ID.equals(message.getJMSCorrelationID())) {
                final String messageText = ((TextMessage) message).getText();
                LOGGER.info("Received reply message: {}", messageText);
                break;
            } else {
                LOGGER.warn("Unknown message: {}", message);
            }
        }
        connection.close();
    }

    public static void main(String[] args) throws Exception {
        String host = System.getProperty("activemq.host", "localhost");
        if (args.length > 0) {
            host = args[0];
        }
        ActiveMQRequest client = new ActiveMQRequest(host);
        client.request();
    }
}
