package cuenen.raymond.broker;

import akka.actor.ActorSystem;
import akka.actor.ExtendedActorSystem;
import akka.camel.CamelExtension;
import akka.camel.ContextProvider;
import java.io.File;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.spring.SpringCamelContext;
import org.apache.xbean.spring.context.ResourceXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class CamelHelper implements ContextProvider {

    public static void initRoutes(ActorSystem system) throws Exception {
        RouteDefinition route = new RouteDefinition();
        route.from("direct:its.requests").id("apiRequests")
                .dynamicRouter().method(LoadBalancer.getLoadBalancer("api-requests"), "next");
        CamelExtension.get(system).context().addRouteDefinition(route);
    }

    @Override
    public DefaultCamelContext getContext(ExtendedActorSystem system) {
        final File camelConfig = new File(System.getProperty("camel.config"));
        SpringCamelContext.setNoStart(true);
        ApplicationContext context = new ResourceXmlApplicationContext(new FileSystemResource(camelConfig));
        return context.getBean("rff-camel-context", DefaultCamelContext.class);
    }
}
