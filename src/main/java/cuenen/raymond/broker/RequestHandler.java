package cuenen.raymond.broker;

import akka.camel.CamelMessage;
import akka.camel.javaapi.UntypedConsumerActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class RequestHandler extends UntypedConsumerActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public String getEndpointUri() {
        return "direct:its.requests." + getSelf().path().name();
    }

    public RequestHandler() {

    }

    @Override
    public void preStart() {
        super.preStart();
        LoadBalancer.getLoadBalancer("api-requests").addEndpoint(getEndpointUri());
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        LoadBalancer.getLoadBalancer("api-requests").removeEndpoint(getEndpointUri());
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof CamelMessage) {
            final CamelMessage camelMessage = (CamelMessage) message;
            final String text = camelMessage.getBodyAs(String.class, getCamelContext());
            log.info("[{}] Received message: {}, sending reply", getSelf().path().name(), text);
            getSender().tell(text.toUpperCase(), getSelf());
        } else {
            unhandled(message);
        }
    }
}
