/*
 * $Revision$
 *
 * Copyright (c) 2008-2014 Vialis BV 
 */
package cuenen.raymond.broker;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.apache.camel.Properties;

public class LoadBalancer {

    private static final Map<String, LoadBalancer> BALANCERS = new ConcurrentHashMap<>();

    public static synchronized LoadBalancer getLoadBalancer(String id) {
        LoadBalancer balancer = BALANCERS.get(id);
        if (balancer == null) {
            balancer = new LoadBalancer();
            BALANCERS.put(id, balancer);
        }
        return balancer;
    }

    private final List<String> endpoints = new CopyOnWriteArrayList<>();

    public void addEndpoint(String uri) {
        endpoints.add(uri);
    }

    public void removeEndpoint(String uri) {
        endpoints.remove(uri);
    }

    private int index = 0;

    public String next(@Properties Map<String, Object> properties) {
        boolean invoked = false;
        Object current = properties.get("invoked");
        if (current != null) {
            invoked = Boolean.parseBoolean(current.toString());
        }
        properties.put("invoked", true);
        if (invoked) {
            return null;
        }
        return nextEndpoint();
    }

    private String nextEndpoint() {
        if (index >= endpoints.size()) {
            index = 0;
        }
        return endpoints.get(index++);
    }
}
