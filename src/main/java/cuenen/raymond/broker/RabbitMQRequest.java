package cuenen.raymond.broker;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RabbitMQRequest {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQRequest.class);
    private static final String QUEUE_NAME = "ITS.Operations";
    private static final String MESSAGE_ID = "rmq-request";

    private final ConnectionFactory connectionFactory = new ConnectionFactory();

    public RabbitMQRequest(String host) {
        connectionFactory.setHost(host);
    }

    public void request() throws Exception {
        final Connection connection = connectionFactory.newConnection();
        final Channel channel = connection.createChannel();
        final String replyQueueName = channel.queueDeclare().getQueue();
        final QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(replyQueueName, true, consumer);

        final BasicProperties props = new BasicProperties.Builder()
                .correlationId(MESSAGE_ID)
                .replyTo(replyQueueName)
                .build();
        channel.basicPublish("", QUEUE_NAME, props, "Request from RabbitMQ".getBytes());

        while (true) {
            final QueueingConsumer.Delivery message = consumer.nextDelivery();
            final String messageText = new String(message.getBody());
            if (MESSAGE_ID.equals(message.getProperties().getCorrelationId())) {
                LOGGER.info("Received reply message: {}", messageText);
                break;
            } else {
                LOGGER.warn("Unknown message: {}", messageText);
            }
        }

        connection.close();
    }

    public static void main(String[] args) throws Exception {
        String host = System.getProperty("rabbitmq.host", "localhost");
        if (args.length > 0) {
            host = args[0];
        }
        RabbitMQRequest client = new RabbitMQRequest(host);
        client.request();
    }
}
