package cuenen.raymond.broker;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActiveMQClient implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActiveMQClient.class);

    private static final int AckMode;
    private static final String TopicName;

    private final boolean transacted = false;
    private final ActiveMQConnectionFactory connectionFactory;

    static {
        TopicName = "its.messages";
        AckMode = Session.AUTO_ACKNOWLEDGE;
    }

    public ActiveMQClient(String host) {
        connectionFactory = new ActiveMQConnectionFactory("tcp://" + host + ":61616");
    }

    public void start() throws Exception {
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(transacted, AckMode);
        final Destination itsMessages = session.createTopic(TopicName);
        final MessageConsumer consumer = session.createConsumer(itsMessages);
        consumer.setMessageListener(this);
        LOGGER.info("Waiting for messages...");
    }

    @Override
    public void onMessage(Message msg) {
        try {
            if (msg instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) msg;
                String messageText = textMessage.getText();
                LOGGER.info("Received message: {}", messageText);
            } else {
                LOGGER.warn("Unknown message: {}", msg);
            }
        } catch (JMSException e) {
            LOGGER.error("Error handling message: " + msg, e);
        }
    }

    public static void main(String[] args) throws Exception {
        String host = System.getProperty("activemq.host", "localhost");
        if (args.length > 0) {
            host = args[0];
        }
        ActiveMQClient client = new ActiveMQClient(host);
        client.start();
    }
}
